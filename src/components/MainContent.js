import React, { Component } from 'react'
import Header from './Header'
import Form from './Form'

class MainContent extends Component {
  render() {
    return (
      <div>
        <Header></Header>
        <Form></Form>
      </div>
    )
  }
}

export default MainContent