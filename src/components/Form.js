import React, { Component } from 'react'
import '../index.css'
import InputText from './InputText'
import signup from './Mobile-login.jpg'
import Gender from './Gender'
import Role from './Role'
import Checkbox from './Checkbox'
import Button from '../Button'
import validator from 'validator'

class Form extends Component {

    constructor(props) {
        super(props)

        this.state = {
            fName: '',
            lName: '',
            email: '',
            age: '',
            password: '',
            repeatPassword: '',
            gender: '',
            role: '',
            tos: false,
            success: {
                display: 'none'
            }
        }

        this.handleSubmit = this.handleSubmit.bind(this)

    }

    handleChange = (event) => {
        const elements = event.target.id
        this.setState({
            [elements]: event.target.value
        })
    }

    handleSubmit(event) {
        event.preventDefault()
        let flag = false
        let validName = false

        if (!validator.isAlpha(this.state.fName)) {
            event.target.fName.style.border = '2px solid red'
            flag = false
        } else {
            event.target.fName.style.border = '2px solid green'
            flag = true
            validName = true
        }

        if (!validator.isAlpha(this.state.lName)) {
            event.target.lName.style.border = '2px solid red'
            flag = false
        } else {
            event.target.lName.style.border = '2px solid green'
            flag = true
        }

        if (!validator.isInt(this.state.age, { min: 18, max: 100 })) {
            event.target.age.style.border = '2px solid red'
            flag = false
        } else {
            event.target.age.style.border = '2px solid green'
            flag = true
        }

        if (!validator.isEmail(this.state.email)) {
            event.target.email.style.border = '2px solid red'
            flag = false
        } else {
            event.target.email.style.border = '2px solid green'
            flag = true
        }

        if (this.state.password === '' || this.state.password.length < 6) {
            event.target.password.style.border = '2px solid red'
            flag = false
        } else {
            event.target.password.style.border = '2px solid green'
            flag = true
        }

        if (this.state.repeatPassword !== this.state.password || this.state.password === '') {
            event.target.repeatPassword.style.border = '2px solid red'
            flag = false
        } else {
            event.target.repeatPassword.style.border = '2px solid green'
            flag = true
        }

        if (this.state.gender === '') {
            event.target.gender.style.border = '2px solid red'
            flag = false
        } else {
            event.target.gender.style.border = '2px solid green'
            flag = true
        }

        if (this.state.role === '') {
            event.target.role.style.border = '2px solid red'
            flag = false
        } else {
            event.target.role.style.border = '2px solid green'
            flag = true
        }

        if (event.target.tos.checked === false) {
            event.target.tos.nextElementSibling.style.color = 'red'
            flag = false
        } else {
            event.target.tos.nextElementSibling.style.color = 'green'
            flag = true
        }

        if (flag && validName) {
            this.setState({
                success: {
                    display: 'block',
                    color: 'green'
                },
            })
        }

    }

    render() {
        return (
            <div className='main-page'>
                <form className='form-container' onSubmit={this.handleSubmit}>
                    <h1 className='main-header'>Sign-Up For Our Newsletter</h1>
                    <div className='form'>
                        <div className='horizontal'>
                            <InputText
                                name="First Name"
                                id="fName"
                                type="text"
                                onChange={this.handleChange}
                                icon="fa-solid fa-font"
                                label="*Only alphabets">
                            </InputText>
                            <InputText
                                name="Last Name"
                                id="lName"
                                type="text"
                                onChange={this.handleChange}
                                icon="fa-solid fa-font"
                                label="*Only alphabets">
                            </InputText>
                        </div>
                        <div className='horizontal'>
                            <InputText
                                name="Age"
                                id="age"
                                type="text"
                                onChange={this.handleChange}
                                icon="fa-solid fa-arrow-up-1-9"
                                label="*Valid number between 18 and 100">
                            </InputText>
                            <InputText
                                name="Email"
                                id="email"
                                type="text"
                                onChange={this.handleChange}
                                icon = "fa-solid fa-at"
                                label="*Valid email">
                            </InputText>
                        </div>
                        <div className='horizontal'>
                            <InputText
                                name="Password"
                                id="password"
                                type="password"
                                onChange={this.handleChange}
                                icon="fa-solid fa-key"
                                label="*Minimum 6 characters">
                            </InputText>
                            <InputText
                                name="Repeat Password"
                                id="repeatPassword"
                                type="password"
                                onChange={this.handleChange}
                                icon="fa-solid fa-key"
                                label="*Passwords must match "></InputText>
                        </div>
                        <div className='horizontal'>
                            <Gender
                                name="Gender"
                                onChange={this.handleChange}
                                id="gender">
                            </Gender>
                            <Role
                                name="Role"
                                onChange={this.handleChange}
                                id="role">
                            </Role>
                        </div>
                    </div>
                    <Checkbox id='tos' name='tos' onChange={this.handleChange}></Checkbox>
                    <Button></Button>
                    <h3 style={this.state.success}>Congratulations! You're Awesome!</h3>
                </form>
                <div className='image-container'>
                    <img src={signup} alt='signup' className='login-image' />
                </div>
            </div>
        )
    }
}

export default Form