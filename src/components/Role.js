import React, { Component } from 'react'
import '../index.css'

class Role extends Component {
    render() {
        return (
            <div>
                <label className='label'>{this.props.name}</label>
                <select name="role" id="role" className='select' onChange={this.props.onChange}>
                    <option value="select">Select</option>
                    <option value="developer">Developer</option>
                    <option value="senior developer">Senior Developer</option>
                    <option value="lead engineer">Lead Engineer</option>
                    <option value="cto">CTO</option>
                </select>
            </div>
        )
    }
}

export default Role