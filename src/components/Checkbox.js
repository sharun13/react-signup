import React, { Component } from 'react'
import '../index.css'

export class Checkbox extends Component {
  render() {
    return (
      <div className='checkbox-1' id='checkbox-1'>
        <input type="checkbox" id="tos" name="tos" />
        <label htmlFor="tos" className='label'>I agree to the terms of service</label>
      </div>
    )
  }
}

export default Checkbox