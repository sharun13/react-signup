import React, { Component } from 'react'
import '../index.css'

class Gender extends Component {
    render() {
        return (
            <div>
                <label className='label'>{this.props.name}</label>
                <select name="gender" id="gender" className='select' onChange={this.props.onChange}>
                    <option value="select">Select</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="transgender">Transgender</option>
                </select>
            </div>
        )
    }
}

export default Gender