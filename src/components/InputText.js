import React, { Component } from 'react'
import '../index.css'

class InputText extends Component {
  render() {
    return (
      <div className='input-container'>
        <input type={this.props.type} id={this.props.id} placeholder={this.props.name} className="input" onChange={this.props.onChange}/>
        <i className={this.props.icon}></i>
        <label className='label'>{this.props.label} {this.props.logo}</label>
      </div>
    )
  }
}

export default InputText