import './index.css'
import MainContent from './components/MainContent';

function App() {
  return (
    <div className="App">
      <MainContent></MainContent>
    </div>
  );
}

export default App;
