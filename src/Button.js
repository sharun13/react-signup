import React, { Component } from 'react'

export class Button extends Component {
  render() {
    return (
      <div>
        <button className='button' type='submit'>Submit</button>
      </div>
    )
  }
}

export default Button